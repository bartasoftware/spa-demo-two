import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'spa-child-one';

  constructor()
  {
    window.onmessage = (e) => {
      if (e.origin.indexOf(location.host) >=0)
      {
        //Ignore messages from self
        return;
      }
      console.log("Got message in child: ", e)
    };
  }

  sendToParent()
  {
    window.top.postMessage("Hello Parent!", '*');
  }
}
