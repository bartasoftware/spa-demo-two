import {Component, SecurityContext, ViewChild} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('iframeContent')
  iframeElement;

  iframeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('http://localhost:4201/');

  loading: boolean = true;

  constructor(private domSanitizer: DomSanitizer) {
    window.onmessage = (e) => {
      if (e.origin.indexOf(location.host) >=0)
      {
        //Ignore messages from self
        return;
      }
      console.log("GOT ONMESSAGe in parent: ", e);
    }

  }

  childOne() {
    console.log('Start');
    this.loading = true;
    this.iframeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('http://localhost:4201/');
  }

  childTwo() {
    console.log('Start');
    this.loading = true;
    this.iframeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('http://localhost:4202/');
  }

  loadFinished() {
    console.log('finish');
    this.loading = false;
    this.iframeElement.nativeElement.contentWindow.postMessage('Welcome to SPA', '*');
  }
}
